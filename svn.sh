svnlogs() {
	num=$1
	if [ $# -eq 0 ]; then
		num=50
	fi
	svn log -l $num | sed -n '/NN/,/-----$/ p'
}

svnadd() {
	if [ $# -eq 1 ]; then
		svn st | grep '^?' | awk '{print $2"@"}' | grep $1 | xargs svn add
	else
		svn st | grep '^?' | awk '{print $2"@"}' | xargs svn add
	fi
}

svndelete() {
	if [ $# -eq 1 ]; then
                svn st | grep '^!' | awk '{print $2"@"}' | grep $1 | xargs svn delete
        else
                svn st | grep '^!' | awk '{print $2"@"}' | xargs svn delete
        fi
}
