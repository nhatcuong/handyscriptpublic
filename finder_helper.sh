function copy_if_file_exist_in_dest {
	CP_FROM_DIR=$1
	CP_TO_DIR=$2
	recursive=$3
	if [ ! -d $CP_FROM_DIR/added ];
		then
		mkdir $CP_FROM_DIR/added
	fi
	for file in $CP_FROM_DIR/*; do
		if [ ! -f $file ];
			then
			continue
		fi
		file_name=${file##*/}
		if [ $recursive = "-r" ]
			then
			file_path_in_dest="$(find $CP_TO_DIR -name $file_name)"
		else
			file_path_in_dest=$CP_TO_DIR/$file_name
		fi
		if [ -f $file_path_in_dest ];
			then
			echo $file_path_in_dest
			cp $file $file_path_in_dest
			mv $file $CP_FROM_DIR/added/
		fi
	done
}
